<?php

namespace Drupal\bothive\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;

/**
 * Class BothiveController.
 *
 * Controller to add & intitialise the Bothive chatbot widget.
 */
class BothiveController {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $manager;

  /**
   * BothiveController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface.
   * @param \Drupal\Core\Executable\ExecutableManagerInterface $manager
   *   The ConditionManager for building the visibility UI.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ExecutableManagerInterface $manager) {
    $this->configFactory = $config_factory->get('bothive.configuration');
    $this->manager = $manager;
  }

  /**
   * Attaches the Bothive widget and initialises it.
   *
   * @param array $page
   *   The whole page array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function attachAndInitialise(array &$page) {
    /** @var \Drupal\system\Plugin\Condition\RequestPath $condition */
    $condition = $this->manager->createInstance('request_path');
    $condition->setConfiguration($this->configFactory->get('request_path'));

    if ($this->manager->execute($condition) && !empty($this->configFactory->get('api_key'))) {
      // Attach the general widget code.
      $page['#attached']['library'][] = 'bothive/bothive-widget';
      // Initialise the chatbot and pass our settings.
      $page['#attached']['library'][] = 'bothive/bothive-initialisation';
      $page['#attached']['drupalSettings']['bothive']['apiKey'] = $this->configFactory->get('api_key');
      $page['#attached']['drupalSettings']['bothive']['logging'] = $this->configFactory->get('logging');
      $page['#attached']['drupalSettings']['bothive']['hidden'] = $this->configFactory->get('hidden');
    }
  }

}
