<?php

namespace Drupal\bothive\Form;

use Drupal\Component\Plugin\Factory\FactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BothiveConfigurationForm.
 *
 * Everything related to the Bothive configuration form.
 */
class BothiveConfigurationForm extends ConfigFormBase {

  /**
   * The request_path condition plugin.
   *
   * @var \Drupal\system\Plugin\Condition\RequestPath
   */
  protected $condition;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bothive.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bothive_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, FactoryInterface $plugin_factory) {
    parent::__construct($config_factory);
    $this->condition = $plugin_factory->createInstance('request_path');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.condition')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bothive.configuration');

    // Set the default condition configuration.
    $this->condition->setConfiguration($config->get('request_path'));

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('The Bothive API key. You can find this in the Bothive Dashboard under Settings » General.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
    ];

    $form['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Logging'),
      '#description' => $this->t('Shows informative logs in the browser console. Errors are always shown regardless of this option.'),
      '#default_value' => $config->get('logging'),
    ];

    $form['hidden'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hidden'),
      '#description' => $this->t('Loads the chatbot invisible.'),
      '#default_value' => $config->get('hidden'),
    ];

    $form += $this->condition->buildConfigurationForm($form, $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->condition->submitConfigurationForm($form, $form_state);
    $this->config('bothive.configuration')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('logging', $form_state->getValue('logging'))
      ->set('hidden', $form_state->getValue('hidden'))
      ->set('request_path', $this->condition->getConfiguration())
      ->save();
  }

}
