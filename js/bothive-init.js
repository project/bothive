/**
 * @file
 * Initialises the Bothive chatbot widget.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the bothive initialisation behaviour.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Actually attaches the Bothive chatbot initialisation.
   */
  Drupal.behaviors.initialiseBothive = {
    attach: function (context, settings) {
      Bothive.widget.init({ apiKey: settings.bothive.apiKey, logging: settings.bothive.logging, hidden: settings.bothive.hidden});
    }
  };

})(jQuery, Drupal, drupalSettings);
