# Bothive Chatbot

The Bothive Chatbot module embeds the chatbot widget from https://bothive.be and
allows you to configure it.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/bothive).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/bothive).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.

You do need to have a valid API key and have your website whitelisted
as an allowed domain to be able to embed the chatbot. More information
can be found under the section configuration.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure the user permissions in Administration » People » Permissions:

   - administer bothive chatbot

   - Users with this permission will be able to change any of the configuration
    settings provided by the module.

2. Customize the Bothive settings in `Administration » Configuration » System » Bothive`

   - The module expects a valid API key from https://bothive.be. You can find
     this API key under Settings » General. While you are on this page also make
     sure to whitelist all the domains required for your website.

   - The logging option will output some additional logging information in the
     browser console.

   - The hidden option will still initialise the widget, but will not show it.
     This would enable you to write custom JavaScript code to open the widget
     with custom defined logic. More information about this can be found on the
     documentation page online.

   - The pages (request path) option allows you to show the chatbot only on
     specific pages. The negate option allows you to reverse the logic. This
     logic is inherited from the Drupal core request path condition.


## Troubleshooting

  * If the chatbot isn't being displayed enable the logging function under
    Administration » Configuration » System » Bothive and open the browser
    console to check for errors.


## FAQ

**Q: I enabled the module but can't see the chatbot.**

**A:** Make sure you configured the module correctly (API key & allowed pages). Your
   site also needs to be whitelisted in order to initialise the chatbot.

**Q: Is the Bothive chatbot free of charge?**

**A:** You can create a free account and use the basic features. However, you will
   be limited in the amount of integrations, triggers and flows you can
   configure. For more information about pricing use the contact form at
   https://bothive.be/contact


## Maintainers

- Bram Driesen - [BramDriesen](https://www.drupal.org/user/3383264)

This project has been sponsored by:

- [Tobania](https://www.tobania.be/en-gb/)
